package com.test.reactiveuitest

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.ref.WeakReference

class MainPresenter(view: MainView,
                    private val repository: Repository) {

    private var viewReference: WeakReference<MainView> = WeakReference(view)

    private val view: MainView?
        get() = viewReference.get()

    private var loadDisposable: Disposable? = null
    private lateinit var refreshes: Observable<Any>

    fun setSwipeObservable(refreshes: Observable<Any>) {
        this.refreshes = refreshes
    }

    fun onCreate() {
        loadDisposable = refreshes.startWith(Any())
                .observeOn(Schedulers.io())
                .flatMap {
                    getResultModel().toObservable()
                            .startWith(MainViewModel(progress = true))
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view?.render(it) }, { it.printStackTrace() })
    }

    private fun getResultModel(): Single<MainViewModel> {
        return repository.getModel()
                .map { MainViewModel(result = it) }
                .doOnError { it.printStackTrace() }
                .onErrorReturn { MainViewModel(error = it.message) }
    }

    fun onDestroy() {
        loadDisposable?.dispose()
    }

    class MainViewModel(var error: CharSequence? = null,
                        var result: Any? = null,
                        var progress: Boolean = false)
}
