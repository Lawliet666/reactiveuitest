package com.test.reactiveuitest

import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit

class MainRepository : Repository {
    private val random = Random(System.currentTimeMillis())

    override fun getModel(): Single<Any> {
        val number = random.nextInt(2)
        val isError = number == 1
        return if (isError) {
            Single.error<Any>(RuntimeException("error"))
                    .delaySubscription(2000, TimeUnit.MILLISECONDS)
        } else {
            Single.just(Any())
                    .delay(2000, TimeUnit.MILLISECONDS)
        }
    }
}