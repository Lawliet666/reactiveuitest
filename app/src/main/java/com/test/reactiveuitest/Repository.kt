package com.test.reactiveuitest

import io.reactivex.Single

interface Repository {
    fun getModel(): Single<Any>
}