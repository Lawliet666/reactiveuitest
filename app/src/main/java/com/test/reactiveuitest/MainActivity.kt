package com.test.reactiveuitest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {
    private lateinit var mainPresenter: MainPresenter
    private val repository = MainRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainPresenter = MainPresenter(this, repository)
        mainPresenter.setSwipeObservable(RxSwipeRefreshLayout.refreshes(swipeRefreshLayout))
        mainPresenter.onCreate()
    }

    override fun onDestroy() {
        mainPresenter.onDestroy()
        super.onDestroy()
    }

    override fun render(viewModel: MainPresenter.MainViewModel) {
        val text = when {
            viewModel.error != null -> {
                swipeRefreshLayout.isRefreshing = false
                "Ошибка:$viewModel.error"
            }
            viewModel.progress -> {
                swipeRefreshLayout.isRefreshing = true
                "Загрузка пожалуйста подождите"
            }
            viewModel.result != null -> {
                swipeRefreshLayout.isRefreshing = false
                "Результат ${viewModel.result}"
            }
            else -> {
                swipeRefreshLayout.isRefreshing = false
                "Неизвестное состояние"
            }
        }

        label.text = text
    }
}
