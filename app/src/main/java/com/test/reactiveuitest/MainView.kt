package com.test.reactiveuitest

interface MainView {
    fun render(viewModel: MainPresenter.MainViewModel)
}